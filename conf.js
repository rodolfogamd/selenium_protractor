exports.config = {
    multiCapabilities: [
        {
            'browserName': 'firefox'
        }, {
            'browserName': 'chrome'
        }
    ],
    seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: ['./**/*.spec.js'],
    framework: 'jasmine2',
    onPrepare: function () {
        var Jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter');
        var folderName = new Date().toLocaleString().replace(' ', '_');
        jasmine.getEnv().addReporter(
            new Jasmine2HtmlReporter({
                savePath: './test_reports/' + folderName + '/',
                filePrefix: 'index'
            })
        );
        //this allows to test non-angular applicaitons
        browser.ignoreSynchronization = true;
    },
    suites: {
        google: './specs/google.spec.js',
        chosen: './specs/chosen.spec.js'
    }
};