module.exports = function () {
    this.searchBox = element(by.id('lst-ib'));
    this.searchButton = element(by.name('btnG'));
    this.resultStats = element(by.id('resultStats'));
    this.imagesLink = element(by.xpath(".//*[@id='hdtb-msb']/div[2]/a"));
    this.image = element.all(by.css('.rg_di.rg_bx.rg_el.ivg-i')).get(0);

    this.get = function () {
        browser.get('http://google.com/');
    }

    this.getTitle = function () {
        return browser.getTitle();
    }

    this.search = function (searchTerm) {
        this.searchBox.sendKeys(searchTerm);
        this.searchButton.click();
        this.waitForElement(this.resultStats);
    }

    this.getResultsText = function () {
        return this.resultStats.getText();
    }

    this.changeToImagesResults = function () {
        return this.imagesLink.click();        
    }

    this.hasImages = function () {
        return this.waitForElement(this.image);
    }

    this.waitForElement = function (element) {
        var until = protractor.ExpectedConditions;
        return browser.wait(until.presenceOf(element), 10000, 'Element taking too long to appear in the DOM');
    }
}